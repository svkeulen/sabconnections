#!/usr/bin/python
import sys
import os
import urllib2
import json
import string
import hashlib
import math

# Get the api key from sabnzbd.ini or the web interface
apikey = "1234567890abcdef1234567890abcdef"

# Name of the Usenet server in your config
newsreader = "MyNewsReader"

# Create a unique secret seed. Use the same secret for everyone in your group
# Also change this in the remote PHP script!
secret = "MyRandomStringOfCharacters"

# URL to the remote PHP script that functions as a database
remoteURL = "http://my.website.com/sab.php"

# Maximum number of lines to share
maxConnections = 8

# Local URLs for sabnzbd
statusURL = "http://localhost:8080/api?mode=qstatus&output=json&apikey=" + apikey
configURL = "http://localhost:8080/api?mode=set_config&section=servers&keyword=" + newsreader + "&apikey=" + apikey

# Ask local sabnzbd if it's busy downloading
def getLocalStatus():
    try:
        response = urllib2.urlopen(statusURL).read()
    except URLError as e:
        print "Sabnzbd API error: " + e.reason

    sabnzbdStatus = json.loads(response)

    if len(sys.argv) > 1 and sys.argv[1] == "force":
        sabnzbdStatus["state"] = "Downloading"

    return 1 if sabnzbdStatus["state"] == "Downloading" else 0

# Get remote status and update it with our local status
def checkConnections():
    try:
        response = urllib2.urlopen(getRemoteURL()).read()
    except URLError as e:
        print "Couldn't get remote status: " + e.reason

    statuses = json.loads(response)

    downloaders = 0
    for key, value in statuses.iteritems():
        downloaders += value

    if localStatus == 0:
        # We're not downloading, so set connections to zero
        return 0
    if localStatus == 1 and downloaders == 0:
        # We're the only one downloading
        return maxConnections
    else:
        # Someone else is downloading too, divide up the available connections
        return int(math.floor(maxConnections / downloaders))

def getRemoteURL():
    meHash = hashlib.md5(me + str(localStatus) + secret).hexdigest()
    return remoteURL + "?meh=" + meHash + "&me=" + me + "&v=" + str(localStatus)

# Create unique indentifier from the api key and the seed
me = hashlib.md5(apikey + secret).hexdigest()
localStatus = getLocalStatus()

# Check how many connections we need
connections = checkConnections()

# Tell sabnzbd to use the new number of available connections
try:
    response = urllib2.urlopen(configURL + "&connections=" + str(connections)).read()
except urllib2.URLError as e:
    print "Sabnzbd API error: " + e.reason
