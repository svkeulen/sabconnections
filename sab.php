<?php
// Be warned: there is no purging of inactive downloaders

$secret = "MyRandomStringOfCharacters";
$filename = "sabstatus.txt";

$me = $_GET['me'];
$meHash = $_GET['meh'];
$meStatus = $_GET['v'];

if($meHash != md5($me . $meStatus . $secret)) die();

$status = json_decode(file_get_contents($filename), true);
$status[$me] = (int)$meStatus;

$status = json_encode($status);

file_put_contents($filename, $status);

die($status);